var User = require('../models/User');
var jwt = require('jsonwebtoken');
var config = require('../config')();
var _ = require('lodash');


class UserController {

    login(req, res, next) {
        if (req.user) {
            res.status(200).json({success: true, message: 'You already logged in!'});
            next();
        } else {
            res.status(404).json({success: false, message: 'user not found'});
            next();
        }
    }

    logout(req, res, next) {
        if (req.user) {
            res.setHeader('x-authorization-token', null);
            res.status(200).json({success: true, message: 'You already log out in!'});
            next();
        } else {
            res.status(404).json({success: false, message: 'user not found'});
            next();
        }
    }

    registration(req, res, next) {

        var newUser = new User({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            username: req.body.username,
            password: req.body.password,
            email: req.body.email,
            gender: req.body.gender,
            status: 0
        });
        newUser.save(function (err, user) {
            var messages = '';
            if (err) {
                res.status(400).json({success: false, message: err});
                next();
            }
            res.mailer.send('email-verification', {
                to: user.email,
                subject: 'Verification Email',
                otherProperty: {
                    site_url: req.headers.origin,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    return_url: req.headers.origin + '/activation/' + user._id
                }
            }, function (err) {
                if (err) {
                    messages = 'There was an error sending the email';
                    res.json({success: true, message: 'Successful created new user.' + messages});
                    next();
                }
                messages = 'The verification email sent, please check your email address.';
                res.json({success: true, message: 'Successful created new user.' + messages});
                next();
            });
        });
    }

    activate(req, res, next) {
        if (!req.params.id) {
            return res.json({success: false, message: "Unknown user."});
            next();
        }
        var id = req.params.id;

        User.findById(id, function (error, user) {
            if(error)
                throw error;

            user.status = "1";

            user.save(function (error, user) {
                if (error)
                    throw error;
                if (!user) {
                    res.json({success: false, message: 'User not found'});
                    next();
                } else {
                    res.json({success: true, message: 'The profile successfully activated!'});
                    next();
                }
            });
        });
    }

    auth(req, res, next) {
        if (!req.body.email || !req.body.password) {
            return res.json({success: false, message: "All fields are required."});
            next();
        }
        User.findOne({
            email: req.body.email
        }).select('+password').exec(
            function (err, user) {
                if (err) {
                    throw err;
                }
                if (!user) {
                    res.send({success: false, message: 'Authentication failed. Wrong email/password.'});
                    next();
                } else {
                    user.comparePassword(req.body.password, function (err, isMatch) {
                        if (isMatch && !err) {
                            user.checkUserStatus(function (isActive) {
                                if (isActive.status) {
                                    // if user is found and password is right create a token
                                    var _user = JSON.parse(JSON.stringify(user));

                                    var token = jwt.sign(_.omit(_user, 'password'), config.jwt_secret, {expiresIn: config.session_life_time});

                                    // return the information including token as JSON
                                    res.setHeader('x-authorization-token', token);
                                    res.json({
                                        success: true,
                                        user: _.omit(_user, 'password')
                                    });
                                    next();
                                } else {
                                    res.json({success: isActive.status, message: isActive.message});
                                    next();
                                }
                            });
                        } else {
                            res.json({success: false, message: 'Authentication failed. Wrong email/password.'});
                            next();
                        }
                    });
                }
            });
    }

    resetPassword(req, res, next) {
        if (!req.body.password) {
            return res.json({success: false, message: "The Password fields is required."});
            next();
        }
        if (!req.params.id) {
            return res.json({success: false, message: "Unknown user."});
            next();
        }
        var id = req.params.id;
        User.findById(id, function (error, user) {
            user.password = req.body.password;
            user.save(function (error, user) {
            });
            if (error)
                throw error;
            if (!user) {
                res.json({success: false, message: 'User not found'});
                next();
            } else {
                var messages = '';
                res.mailer.send('email-reset', {
                    to: user.email,
                    subject: 'Reset Password',
                    otherProperty: {
                        site_url: 'http://application.com',
                        first_name: user.first_name,
                        last_name: user.last_name,
                        username: user.username,
                        password: req.body.password
                    }
                }, function (err) {
                    if (err) {
                        messages = 'There was an error sending the email';
                        return res.json({success: true, message: 'The password successfully updated!' + messages});
                        next();
                    }
                    messages = 'Please check your email address for seeing new password.';
                    res.json({success: true, message: 'The password successfully updated!' + messages});
                    next();
                });
            }
        });
    }

    forgotPassword(req, res, next) {
        if (!req.body.email) {
            return res.json({success: false, message: "The Email fields is required."});
            next();
        }
        User.findOne({email: req.body.email})
            .lean()
            .exec(function (err, user) {
                if (err) {
                    throw err
                }
                if (!user) {
                    res.json({success: false, message: 'User not found'});
                    next();
                } else {
                    res.mailer.send('email-forgot', {
                        to: user.email,
                        subject: 'Forgot Password',
                        otherProperty: {
                            first_name: user.first_name,
                            last_name: user.last_name,
                            return_url: req.headers.origin + '/reset-password/' + user._id
                        }
                    }, function (err) {
                        if (err) {
                            return res.json({success: false, message: 'There was an error sending the email'});
                            next();
                        }
                        res.json({success: true, message: 'Email Sent'});
                        next();
                    });
                }
            });

    }

    update(req, res, next) {
        if (!req.params.id) {
            return res.json({success: false, message: "Unknown user."});
            next();
        }
        var id = req.params.id;

        User.findById(id, function (error, user) {
            if(error)
                throw error;

            user.first_name = req.body.first_name;
            user.last_name = req.body.last_name;
            user.username = req.body.username;
            user.password = req.body.password;
            user.email = req.body.email;
            user.gender = req.body.gender;

            user.save(function (error, user) {
                if (error)
                    throw error;
                if (!user) {
                    res.json({success: false, message: 'User not found'});
                    next();
                } else {
                    res.json({success: true, message: 'The profile successfully updated!'});
                    next();
                }
            });
        });
    }
}

module.exports = new UserController();