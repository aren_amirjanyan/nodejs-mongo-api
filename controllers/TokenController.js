var Application = require('../models/Application');
var jwt = require('jsonwebtoken');
var config = require('../config')();
var _ = require('lodash');


class TokenController {

    get(req, res, next) {
        if (!req.params.app_id && !req.body.app_id) {
            return res.json({success: false, message: "The application ID required!"});
            next();
        }
        var app_id = req.params.app_id || req.body.app_id;

        Application.findById(app_id, function (error, application) {
            if (error)
                throw error;
            if (!application) {
                res.send({success: false, message: 'The application not found!'});
                next();
            } else {
                var _application = JSON.parse(JSON.stringify(application));

                var token = jwt.sign(_.omit(_application, 'secret'), config.jwt_secret, { expiresIn: config.session_life_time });

                application.secret = token;

                application.save(function (error, updatedApplication) {
                    if (error)
                        throw error;
                    if (!updatedApplication) {
                        res.send({success: false, message: 'Can not update token in application'});
                        next();
                    } else {
                        res.setHeader('x-api-token', updatedApplication.secret);
                        res.send({success: true,token:updatedApplication.secret});
                        next();
                    }
                });
            }
        });

    }
}

module.exports = new TokenController();