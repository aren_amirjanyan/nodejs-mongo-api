var securityAPI = require('../helpers/security-api');
var _ = require('lodash');

module.exports = function (req, res, next) {
    res.deny = function (from) {
        this.status(403).json({statusCode: 403, message: from})
    };

    var token = req.headers['x-api-token'];
    if (token) {
        securityAPI.validateToken(token)
            .then(function (data) {
                res.setHeader('x-api-token', data.token);
                next();
            })
            .catch(function (reason) {
                res.deny(reason)
            })
    } else {
        return res.json({success:false,message:'Access denied'});
        next()
    }
};