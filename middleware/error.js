module.exports = function (app) {
    app.use(function (error, req, res, next) {
        res.status(500).json({success: false, message: 'Something went wrong.'})
    })
};