var security = require('../helpers/security');
var _ = require('lodash');

module.exports = function (req, res, next) {
    res.deny = function (from) {
        console.log('request denied from ' + from);
        this.status(403).json({statusCode: 403, message: from})
    };

    if (req.headers['x-authorization-token']) {
        security.validateToken(req.headers['x-authorization-token'])
            .then(function (data) {
                res.setHeader('x-authorization-token', data.token);
                req.user = data.user;
                next()
            })
            .catch(function (reason) {
                res.deny(reason)
            })
    } else {
        return res.json({success:false,message:'Access denied'});
        next();
    }
};