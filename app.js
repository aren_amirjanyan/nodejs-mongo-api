var express = require('express');

var expressJwt = require('express-jwt');

var jwt = require('jsonwebtoken');

var config = require('./config')();

var mailer = require('express-mailer');

require('jade');


var app = express();

app.set('port',process.env.PORT || 3000);

var bodyParser = require('body-parser');

app.use(bodyParser.json());

mailer.extend(app,config.mail);

app.set('views', __dirname + '/email_templates');

app.set('view engine', 'jade');

require('./middleware')(app);

var routes = require('./routes');

require('./middleware/error')(app);

app.use('/api/v1',routes);

app.listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});

