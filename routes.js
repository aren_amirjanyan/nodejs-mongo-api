var express = require('express');
var userCtrl = require('./controllers/UserController');
var tokenCtrl = require('./controllers/TokenController');
var auth = require('./middleware/auth');
var api = require('./middleware/api');

var router = express.Router();

router.post('/auth',userCtrl.auth);

/**
 * Using API Middleware
 */
router.post('/registration',api,userCtrl.registration);
router.put('/activation/:id',api,userCtrl.activate);
router.post('/forgot-password',api,userCtrl.forgotPassword);
router.put('/reset-password/:id',api,userCtrl.resetPassword);
router.put('/profile-update/:id',api,userCtrl.update);


/**
 * Using AUTH Middleware
 */

router.post('/login',auth,userCtrl.login);
router.get('/logout',auth,userCtrl.logout);

/**
 * Get Token
 */
router.get('/getToken/:app_id',tokenCtrl.get);
router.post('/getToken',tokenCtrl.get);


module.exports = router;