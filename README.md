# NodeJS, Mongo, Express REST API #

The project is REST API with node js (Express), mongo 

### How run REST API ? ###

* Clone repository - [git clone https://aren_amirjanyan@bitbucket.org/aren_amirjanyan/nodejs-mongo-api.git](git clone https://aren_amirjanyan@bitbucket.org/aren_amirjanyan/nodejs-mongo-api.git)
* npm install
* check /configs/index.js and change with your credentials
* npm start

### Documentation of REST API
[https://bitbucket.org/aren_amirjanyan/nodejs-mongo-api/wiki/Home](https://bitbucket.org/aren_amirjanyan/nodejs-mongo-api/wiki/Home)