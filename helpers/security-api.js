var config = require('../config')();
var jwt = require('jsonwebtoken');
var Application = require('../models/Application');
var _ = require('lodash');

module.exports = {
    validateToken: function (oldToken) {
        return new Promise(function (resolve, reject) {
            try {
                var decodedData = jwt.verify(oldToken, config.jwt_secret);
            } catch (error) {
                reject(error);
            }
            Application.findById(decodedData._id,function(error,application){
                if (error) {
                    throw error
                }
                if (!application) {
                    reject('Application not found')
                } else {
                    var _application = JSON.parse(JSON.stringify(application));

                    var newToken = jwt.sign(_.omit(_application), config.jwt_secret, { expiresIn: config.session_life_time });

                    resolve({token: newToken})
                }
            });
        })
    }
};