var config = require('../config')();
var jwt = require('jsonwebtoken');
var User = require('../models/User');

module.exports = {
    validateToken: function (oldToken) {
        return new Promise(function (resolve, reject) {
            try {
                var decodedData = jwt.verify(oldToken, config.jwt_secret);
            } catch (error) {
                reject('decode error');
            }
            User.findOne({email: decodedData.email})
                .lean()
                .exec(function (err, user) {
                    if (err) {
                        throw err
                    }
                    if (!user) {
                        reject('User not found')
                    } else {
                        var newToken = jwt.sign(user, config.jwt_secret, { expiresIn: config.session_life_time });
                        resolve({token: newToken, user: user})
                    }
                })
        })
    }
};