var mongoose = require('./connection');
var passwordHash = require('password-hash');
var ObjectId = mongoose.Schema.ObjectId;

var userSchema = new mongoose.Schema({
    id: String,
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        select: false
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    gender: {
        type: String,
        enum: ['male', 'female'],
        required: true
    },
    status: {
        type: String,
        enum: ['0', '1'],
        required: true
    },
    avatarUrl: String,
    created: Date
});


userSchema.methods.comparePassword = function (password, callback) {
    if (passwordHash.verify(password, this.password)) {
        return callback(null, true)
    } else {
        return callback('Passwords doesn\'t match')
    }
};
userSchema.methods.checkUserStatus = function(callback){
    if(this.status == '1')
      return callback({status:true,message:'The account active already'});
    else
      return callback({status:false,message:'The account not active yet! Please check your email.'});
};

userSchema.pre('save', function (next) {
    var User = this;
    if (this.isModified('password') || this.isNew) {
        User.password = passwordHash.generate(User.password);
    }
    return next();
});

var User = mongoose.model('User', userSchema);
module.exports = User;