var mongoose = require('./connection');
var passwordHash = require('password-hash');
var jwt = require('jsonwebtoken');
var ObjectId = mongoose.Schema.ObjectId;

var userSchema = new mongoose.Schema({
    id: String,
    name: {
        type: String,
        required: true
    },
    secret: {
        type: String,
        unique: true,
        required: true
    },
    created: Date
});

var Application = mongoose.model('Application', userSchema);
module.exports = Application;