var gulp = require('gulp');
var server = require('gulp-express');

gulp.task('start', function () {
    // Start the server at the beginning of the task
    server.run(['app.js']);
    gulp.watch(['app.js', 'controllers/**/*.js','models/**/*.js','middleware/**/*.js','helpers/**/*.js','email_templates/**/*.jade'], [server.run]);
});