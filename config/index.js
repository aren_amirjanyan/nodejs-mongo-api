var config = {
    local: {
    mode: 'local',
        port: 5000,
        mongodb_url :'mongodb://mongoadmin:mongoadminapi@ds023694.mlab.com:23694',
        mongodb_name :'nodejs_mongo_api',
        jwt_secret:"AwOWQ4NGQwMTE0MjNkYzlmYjYiLCJuYW1lIjoib3JtZW4",
        session_life_time: 600,
        mail:{
        from: '85od500@gmail.com',
            host: process.env.GMAIL_SMTP_HOST || 'smtp.gmail.com',
            secureConnection: true,
            port: process.env.GMAIL_SMTP_PORT || 465,
            transportMethod: process.env.GMAIL_SMTP_METHOD || 'SMTP',
            auth: {
                user: process.env.GMAIL_SMTP_USER || 'test2131415161@gmail.com',
                pass: process.env.GMAIL_SMTP_PASSWORD || 'test1327418'
        }
    }
   },
    production: {
        mode: 'production',
        port: 3000,
        mongodb_url :'mongodb://localhost:27017',
        mongodb_name :'nodejs_mongo_api',
        jwt_secret:"AwOWQ4NGQwMTE0MjNkYzlmYjYiLCJuYW1lIjoib3JtZW4",
        session_life_time: 600,
        mail:{
            from: '85od500@gmail.com',
            host: 'smtp.gmail.com', // hostname
            secureConnection: true, // use SSL
            port: 465, // port for secure SMTP
            transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
            auth: {
                user: 'test2131415161@gmail.com',
                pass: 'test1327418'
            }
        }
    },
    staging: {
        mode: 'staging',
        port: 4000,
        mongodb_url :'',
        mongodb_name :'',
        jwt_secret:"",
        session_life_time: 600
    }
}
module.exports = function(mode) {
    return config[mode || process.argv[2] || 'local'] || config.local;
}